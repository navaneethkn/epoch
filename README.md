# Simple CLI for epoch conversion

## Installation

```
go get gitlab.com/navaneethkn/epoch
```

## Usage

```
$ > epoch
1496837432473930434
```

By default it outputs the current Unix timestamp in nanoseconds. It can be changed to other formats.

Milliseconds:
```
$ > epoch --format msec
1496837487000
```

Seconds:
```
$ > epoch --format sec
1496837525
```

To convert a date to timestamp:

```
$ > epoch --convert "2017-01-01 05:11am"
1483247460000000000
```

