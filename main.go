package main

import (
	"flag"
	"fmt"
	"os"
	"strconv"
	"time"
)

var layout = "2006-01-02 03:04pm (MST)"

func main() {
	outputFormat := flag.String("format", "", "output formats - sec, msec, nsec")
	flag.Parse()

	valueToConvert := flag.Arg(0)
	if valueToConvert != "" {
		v, err := strconv.ParseInt(valueToConvert, 10, 64)
		if err == nil {
			// value looks like a timestamp
			humanDate, err := timestampToHuman(v)
			if err != nil {
				fmt.Printf("error. %v", err)
				os.Exit(1)
			}
			fmt.Printf("%s\n", humanDate.Format(layout))
		} else {
			t, err := humanToTimeStamp(valueToConvert)
			if err != nil {
				fmt.Printf("error. %v", err)
				os.Exit(1)
			}
			printTimeStamp(t, *outputFormat)
		}

	} else {
		// print the current epoch
		printTimeStamp(time.Now(), *outputFormat)
	}
}

func printTimeStamp(t time.Time, format string) {
	var timestamp int64
	if format == "msec" {
		timestamp = t.Unix() * 1000
	} else if format == "sec" {
		timestamp = t.Unix()
	} else {
		timestamp = t.UnixNano()
	}

	fmt.Printf("%d\n", timestamp)
}

func timestampToHuman(timestamp int64) (time.Time, error) {
	valueToConvert := fmt.Sprintf("%d", timestamp)
	if len(valueToConvert) == 13 {
		// timestamp is in milliseconds
		tn := timestamp * 1000000
		return time.Unix(0, tn), nil
	} else if len(valueToConvert) == 19 {
		// timestamp in nano seconds
		return time.Unix(0, timestamp), nil
	}
	// timestamp in seconds
	return time.Unix(timestamp, 0), nil

}

func humanToTimeStamp(humanDate string) (time.Time, error) {
	t, err := time.Parse(layout, humanDate)
	if err != nil {
		return time.Now(), fmt.Errorf("failed to read date format. %v", err)
	}

	return t, nil
}
